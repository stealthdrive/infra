# StealthDrive example service

This project is mainly divided into 3 parts:

- frontend
- alpr
- backend

Each part holds a part of the service, which is described bellow.  
This repository also holds a pipeline to build and push each of the images to this GitLab repositories container registry. As well as an infra folder with docker-compose definitions for an easy launch of the service.

## ALPR

Automatic License Plate Reader is a tool for reading an image and determining the license plate in that image.  

The Docker image for this service is heavily outdated and is there for rebuilt. This takes a lot of time, so images are saved to GitLab container registry to save time in the future.  

The service expects a RabbitMQ instance to read off the file names (prefixes), a Minio bucket to pull the images from and a PostgreSQL instance to save entries to.  
If required, a telegram bot integration is available for mobile notifications.

## Backend

The backend instance is used to manage communication with RabbitMQ and Minio. The backend service provides an API endpoint for uploading the picture.

## Frontend

A minimal NextJS frontend to give a UI for image uploading as well as some useful links.

## Infrastructure

To launch the service, fill in all of the required values in the `.env` file and launch with `docker compose up -d`

## Building each part individually

Taking into consideration that the task was to create a repository for an app with a Dockerfile, each of the parts can be built seperately.  
As for launching them, that is not the case. Only the contents of the Frontend repository can be deployed without any other service.  

Requirements:  

- alpr: RabbitMQ, Minio, Postgres
- backend: RabbitMQ, Minio

### Building frontend

To build the frontend container image run `docker build -t stealthdrive-frontend .`.  
Then to launch the image run `docker run temp`. This will expose a node service on `localhost:3000` with the frontend service.

### Setting variables

Each part of the service contains a `.env.example` file. This file contains all of the used environment variables that can be configured for the service.  
Create a copy of this file called `.env`, set your values and build/run the service.  

**Note:** Single application deployments do not require hostnames set as in the infra directory. This is used specifically for networking in the docker compose network/use case.
